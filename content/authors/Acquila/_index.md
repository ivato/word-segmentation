---
title: Acquila Santos Rocha
role: Undergraduate student at the Federal University of Goiás
bio: ""
interests: null
social:
  - display:
      header: false
    link: http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K2790189Z6
    icon_pack: fas
    icon: creative-commons-by
organizations: []
education:
  courses:
    - course: "Graduação em andamento em Ciências da Computação. OUma estratégia de segurança baseada no gerenciamento de confiança e blockchain para caching em comunicação D2D.
Orientador: Vinicius da Cunha Martins Borges."
      institution: Universidade Federal de Goiás, UFG, Brasil
      year: 2016 
email: ""
superuser: true
user_groups:
  - Researchers
highlight_name: false
---
Graduando em Ciências da Computação pela Universidade Federal de Goiás (UFG), iniciado em 2016/1. Já atuou como monitor de Matemática Discreta (2017/1 a 2017/2) e Análise e Projetos de Algoritmos (2018/1 a 2019/1). Atualmente faz Iniciação Científica em desenvolvimento de Redes 5G no contexto de cenários extremamente densos e heterogêneos (WiSEED). Tem experiência em Ciência da computação com ênfase em Redes de Computadores e Sistemas Distribuídos. Tem interesse em redes 5G, segurança em comunicação D2D, segurança em redes de computadores, comportamento humano, caching e sistemas distribuídos. Ultimamente tem realizado pesquisas na área de Processamento de Linguagem Natural (PLN).

<!--StartFragment-->
http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K2790189Z6
<!--EndFragment-->
