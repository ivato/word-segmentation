---
title: Hugo Alexandre Dantas do Nascimento
role: Professor at Universidade Federal de Goiás
education:
  courses:
    - course: Graduação em Ciências da Computação
      institution: Universidade Federal do Rio Grande do Norte, UFRN, Brasil.
      year: 1994
    - course: " Mestrado em Ciência da Computação (Conceito CAPES 7)."
      institution: Universidade Estadual de Campinas, UNICAMP, Brasil.
      year: 1997
    - course: Doutorado em Computer Science.
      institution: The University of Sydney, U.SYDNEY, Austrália.
      year: 2006
    - course: "Pós-Doutorado. Grande área: Ciências Exatas e da Terra"
      institution: Universidade Federal do Rio Grande do Norte, UFRN, Brasil
      year: 2017
superuser: true
user_groups:
  - Researchers
---
<!--StartFragment-->

Hugo Nascimento é graduado em Ciências da Computação pela Universidade Federal do Rio Grande do Norte (1994), mestre em Ciência da Computação pela Universidade Estadual de Campinas (1997) e PhD in Science pela University of Sydney (2003). Professor associado da Universidade Federal de Goiás, Hugo pesquisa em Visualização de Informações, Otimização Combinatória e Interação Humano-Computador, bem como na integração entre essas áreas. Os seus projetos de pesquisa recentes incluem o desenvolvimento de algoritmos para modelagem, simulação e melhoria das condições do tráfego urbano, o estudo de algoritmos em desenho de grafos e a construção de artefatos interativos para Arte & Tecnologia

<!--EndFragment-->
<!--StartFragment-->
http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4798562T1
<!--EndFragment-->
