---
title: Marcelo Akira
role: Professor at Universidade Federal de Goiás
bio: ""
interests:
  - Artificial Intelligence
social: []
organizations: []
education:
  courses: []
email: ""
superuser: true
user_groups:
  - Researchers
highlight_name: false
---
Marcelo Akira is a professor at Universidade Federal de Goiás. Started lecturing in 2010.