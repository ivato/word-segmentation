---
title: Juliana Resplande Sant'Anna Gomes
role: Undergraduate student at the Federal University of Goiás
bio: ""
interests: null
social:
  - display:
      header: false
    link: http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K2737623H6
    icon_pack: fas
    icon: creative-commons-by
organizations: []
education:
  courses:
    - course: "Graduação em andamento em Ciências da Computação. Ordenação genômica
        por transposições. Orientador: Diane Castonguay."
      institution: Universidade Federal de Goiás, UFG, Brasil
      year: 2017
email: ""
superuser: true
user_groups:
  - Researchers
highlight_name: false
---
Undergraduate student at the Federal University of Goiás with a focus on natural language processing. She studied topics related to applied mathematics in the field of bioinformatics

<!--StartFragment-->
http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K2737623H6
<!--EndFragment-->
