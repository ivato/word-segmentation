---
widget: hero
widget_id: Word Segmentation
headless: true
weight: 10
active: true
design:
  columns: "2"
  background:
    text_color_light: false
    image_darken: 0
---
Word segmentation is the task of introducing spaces between words when they are not explicitly indicated in the text. The project Word Segmentation from IVATO focus on the particular case of hashtag segmentation.

Hashtag segmentation is used in the preprocessing pipelines of social media datasets. Wich precedes sentiment analysis, hate speech detection, or event detection.
The algorithm selected for the segmentation is the GPT-2 and a beam search algorithm to select the two most likely segmentation candidates for a hashtag.
