---
title: Publications
publication_types:
  - "0"
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
date: 2021-02-23T00:22:48.546Z
---
<script src="https://bibbase.org/show?bib=https%3A%2F%2Fgitlab.com%2Fivato%2Fword-segmentation%2F-%2Fraw%2Fmaster%2Fword%2520segmentation.bib&jsonp=1"></script>
